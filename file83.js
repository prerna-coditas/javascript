function CreateUser(firstName, lastName, email, age, address){
    this.firstName = firstName;
    this.lastName = lastName;
    this.email = email;
    this.age = age;
    this.address = address;
}
CreateUser.prototype.about = function(){
    return `${this.firstName} is ${this.age} years old.`;
};
CreateUser.prototype.is22 = function (){
    return this.age >= 22; 
}
CreateUser.prototype.sing = function (){
    return "la la la la ";
}


const user1 = createUser('prerna', 'uprikar', 'prernan@gmail.com', 23, "my address");
const user2 = createUser('kalash', 'uprikar', 'kalash@gmail.com', 20, "my address");
const user3 = createUser('neha', 'pokh', 'neha@gmail.com', 22, "my address");
console.log(user1);
console.log(user1.is22());