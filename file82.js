function createUser(firstName, lastName, email, age, address){
    const user = Object.create(createUser.prototype);
    user.firstName = firstName;
    user.lastName = lastName;
    user.email = email;
    user.age = age;
    user.address = address;
    return user;
}
createUser.prototype.about = function(){
    return `${this.firstName} is ${this.age} years old.`;
};
createUser.prototype.is22 = function (){
    return this.age >= 22; 
}
createUser.prototype.sing = function (){
    return "la la la la ";
}


const user1 = createUser('prerna', 'uprikar', 'prernan@gmail.com', 23, "my address");
const user2 = createUser('kalash', 'uprikar', 'kalash@gmail.com', 20, "my address");
const user3 = createUser('neha', 'pokh', 'neha@gmail.com', 22, "my address");
console.log(user1);
console.log(user1.is22());
