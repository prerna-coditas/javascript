class CreateUser{
    constructor(firstName, lastName, email, age, address){
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.age = age;
        this.address = address;
    }

    about(){
        return `${this.firstName} is ${this.age} years old.`;
    }
    is22(){
        return this.age >= 22;
    }
    sing(){
        return "la la la la ";
    }

}


const user1 = createUser('prerna', 'uprikar', 'prernan@gmail.com', 23, "my address");
const user2 = createUser('kalash', 'uprikar', 'kalash@gmail.com', 20, "my address");
const user3 = createUser('neha', 'pokh', 'neha@gmail.com', 22, "my address");