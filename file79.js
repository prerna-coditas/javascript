const userMethods = {
    about : function(){
        return `${this.firstName} is ${this.age} years old.`;
    },
    is22 : function(){
        return this.age >= 22;
    },
    sing: function(){
        return 'toon na na na la la ';
    }
}
function createUser(firstName, lastName, email, age, address){
    const user = Object.create(userMethods);// {}
    user.firstName = firstName;
    user.lastName = lastName;
    user.email = email;
    user.age = age;
    user.address = address;
    return user;
}

const user1 = createUser('prerna', 'uprikar', 'prernan@gmail.com', 23, "my address");
const user2 = createUser('kalash', 'uprikar', 'kalash@gmail.com', 20, "my address");
const user3 = createUser('neha', 'pokh', 'neha@gmail.com', 22, "my address");
console.log(user1);
console.log(user1.about());
// console.log(user3.sing());