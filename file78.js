const userMethods = {
    about : function(){
        return `${this.firstName} is ${this.age} years old.`;
    },
    is18 : function(){
        return this.age >= 22;
    }
}
function createUser(firstName, lastName, email, age, address){
    const user = {};
    user.firstName = firstName;
    user.lastName = lastName;
    user.email = email;
    user.age = age;
    user.address = address;
    user.about = userMethods.about;
    user.is22 = userMethods.is22;
    return user;
}

const user1 = createUser('prerna', 'uprikar', 'prernan@gmail.com', 23, "my address");
const user2 = createUser('kalash', 'uprikar', 'kalash@gmail.com', 20, "my address");
const user3 = createUser('neha', 'pokh', 'neha@gmail.com', 22, "my address");
console.log(user1.about());
console.log(user3.about());