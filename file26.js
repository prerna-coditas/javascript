 let fruits = ["apple", "mango", "grapes"];
let numbers = [1,2,3,4];
let mixed = [1,2,2.3, "string", null, undefined];
console.log(mixed);
console.log(numbers);
console.log(fruits[2]);


let fruits1 = ["apple", "mango", "grapes"];
let obj = {}; // object literal
console.log(fruits1);
fruits[1] = "banana";
console.log(fruits1);
console.log(typeof fruits1);
console.log(typeof obj);



console.log(Array.isArray(fruits1));
console.log(Array.isArray(obj));