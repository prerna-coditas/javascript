const users = [
    {firstName: "prerna", age: 23},
    {firstName: "kalash", age: 20},
    {firstName: "neha", age: 22},
    {firstName: "nithya", age: 22},
]

const userNames = users.map((user)=>{
    return user.firstName;
});

console.log(userNames);